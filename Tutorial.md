As tecnologias usadas para o desenvolvimento foram o Java JDK 1.8, Maven 3.3.9 e o framework Spring Boot na versão 2, nos testes unitários e de integração fora utilizado o JUnit.

Para iniciar a aplicação é necessário possuir o JAVA JDK 1.8 instalado na máquina, assim como o Maven e versão 3.3.x, assim como as variáveis de ambiente JAVA_HOME e M2_HOME.

Existem váras formas de fazer isso, porém a que eu recomendo é o uso da ferramenta http://jenv.io/ .

Com o jenv instalado no ambiente, seguindo o tutorial super básico do seu site, executaremos os seguintes comandos:

$ jenv install java 1.8.0_91

$ jenv install maven 3.3.9.

Com o Java JDK 1.8 e o Maven instalado, necessitamos apenas ir na pasta raiz do projeto, onde está o arquivo pom.xml, e executar o seguinte comando:

$ mvn spring-boot:run

A aplicação irá iniciar com um banco de dados em memória.

As requisições estão protegidas, para logar na aplicação é necessário realizar uma requisição do tipo POST para o endereço http://localhost:8080/login com o seguinte json no body:

{"username": "admin", "password": "password"}

Após isso devemos adicionar o parâmetro Authorization com o token gerado pela aplicação.

Para executar os testes unitários é só executar o seguinte comando:

$ mvn test

Fora construído um teste de integração nas possíveis requisições do Controller, mais testes poderiam ser feitos para garantir uma cobertura bem maior, mas foram apenas paar demonstração do conhecimento.


Também foi adicionar o teste-trilogo.jar na raiz do repositório para facilitar a execução da aplicação, pois, com isso, é necessário apena do java previamente instalado, ir na pasta onde se encontra 
o "teste-trilogo.jar" e realizar a execução do seguinte comando:

$ java -jar teste-trilogo.jar

A aplicação irá ser executada em localhost:8080