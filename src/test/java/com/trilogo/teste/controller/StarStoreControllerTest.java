package com.trilogo.teste.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.trilogo.teste.DemoApplication;
import com.trilogo.teste.model.CreditCard;
import com.trilogo.teste.model.Product;
import com.trilogo.teste.model.Purchase;
import com.trilogo.teste.model.Transaction;
import com.trilogo.teste.security.filter.UserCredentials;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@WebAppConfiguration
public class StarStoreControllerTest {

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private FilterChainProxy springSecurityFilterChain;
	
	private MockMvc mockMvc;
	
	private String accessToken;

	private Product product;
	
	private Transaction transaction;
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
		
		try {
			this.accessToken = obtainAccessToken("admin", "password");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.product = new Product();
		this.product.setTitle("Blusa do Imperio");
		this.product.setPrice(7990);
		this.product.setZipcode("78993-000");
		this.product.setSeller("João da Silva");
		this.product.setThumbnail("https://cdn.awsli.com.br/600x450/21/21351/produto/3853007/f66e8c63ab.jpg");
		this.product.setDate("26/11/2015");
		
		this.transaction = new Transaction();
		this.transaction.setClientId("7e655c6e-e8e5-4349-8348-e51e0ff3072e");
		this.transaction.setClientName("Luke Skywalker");
		this.transaction.setTotalToPay(1236);
		this.transaction.setCreditCard(new CreditCard());
		this.transaction.getCreditCard().setCard_number("1234123412341234");
		this.transaction.getCreditCard().setValue(7990);
		this.transaction.getCreditCard().setCvv(789);
		this.transaction.getCreditCard().setCard_holder_name("Luke Skywalker");
		this.transaction.getCreditCard().setExp_date("12/04");
		
	}

	@Test
	public void getProductsTest() throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		
	    ResultActions result = mockMvc.perform(get("/starstore/products")
	      .header("Authorization", this.accessToken)
	      .contentType(MediaType.APPLICATION_JSON_UTF8))
	      .andExpect(status().isOk());
	    
	    String jsonResult = result.andReturn().getResponse().getContentAsString();
	    
	    List<Product> productList = mapper.readValue(jsonResult, new TypeReference<List<Product>>(){});

	    assertNotNull(productList.get(0));
	    
	}

	@Test
	public void saveProductTest() throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		
		 ResultActions result = mockMvc.perform(post("/starstore/product")
			      .header("Authorization", this.accessToken)
			      .contentType(MediaType.APPLICATION_JSON_UTF8)
			      .content(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this.product)))
			      .andExpect(status().isOk());
		 
		 String jsonResult = result.andReturn().getResponse().getContentAsString();
		 
		 Product produto = mapper.readValue(jsonResult, Product.class);
		 
		 assertNotNull(produto);
	}
	
	@Test
	public void saveProductErrorTest() throws Exception {
		
		mockMvc.perform(post("/starstore/product")
			      .header("Authorization", this.accessToken)
			      .contentType(MediaType.APPLICATION_JSON_UTF8))
			      .andExpect(status().isBadRequest());
		 
	}
	
	@Test
	public void saveTransactionTest() throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		
		String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this.transaction);
		
		 ResultActions result = mockMvc.perform(post("/starstore/buy")
			      .header("Authorization", this.accessToken)
			      .contentType(MediaType.APPLICATION_JSON_UTF8)
			      .content(jsonInString))
			      .andExpect(status().isOk());
		 
		 String jsonResult = result.andReturn().getResponse().getContentAsString();
		 
		 Transaction transaction = mapper.readValue(jsonResult, Transaction.class);
		 
		 assertNotNull(transaction);
	}
	
	@Test
	public void saveTransactionErrorTest() throws Exception {
		
		 mockMvc.perform(post("/starstore/buy")
			      .header("Authorization", this.accessToken)
			      .contentType(MediaType.APPLICATION_JSON_UTF8))
			      .andExpect(status().isBadRequest());
		 
	}
	
	@Test
	public void getHistoryTest() throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		
	    ResultActions result = mockMvc.perform(get("/starstore/history")
	      .header("Authorization", this.accessToken)
	      .contentType(MediaType.APPLICATION_JSON_UTF8))
	      .andExpect(status().isOk());
	    
	    String jsonResult = result.andReturn().getResponse().getContentAsString();
	    
	    List<Purchase> purchaseList = mapper.readValue(jsonResult, new TypeReference<List<Purchase>>(){});

	    assertNotNull(purchaseList);
	    
	}
	
	@Test
	public void getHistoryByClientIdTest() throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		
		String clientId = this.transaction.getClientId();
		
		mockMvc.perform(post("/starstore/buy")
			      .header("Authorization", this.accessToken)
			      .contentType(MediaType.APPLICATION_JSON_UTF8)
			      .content(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this.transaction)))
			      .andExpect(status().isOk());
		
		this.transaction.setClientId("abc");
		
		mockMvc.perform(post("/starstore/buy")
			      .header("Authorization", this.accessToken)
			      .contentType(MediaType.APPLICATION_JSON_UTF8)
			      .content(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this.transaction)))
			      .andExpect(status().isOk());
	   		
	    ResultActions result = mockMvc.perform(get("/starstore/history/{clientId}", clientId)
	      .header("Authorization", this.accessToken)
	      .contentType(MediaType.APPLICATION_JSON_UTF8))
	      .andExpect(status().isOk());
	    
	    String jsonResult = result.andReturn().getResponse().getContentAsString();
	    
	    List<Purchase> purchaseList = mapper.readValue(jsonResult, new TypeReference<List<Purchase>>(){});

	    assertNotNull(purchaseList);
	    assertThat(purchaseList.size() == 1);
	    assertThat(purchaseList.get(0).getClientId().equals(clientId));
	}
	
	
	
	private String obtainAccessToken(String username, String password) throws Exception {

		UserCredentials user = new UserCredentials();
		user.setUsername(username);
		user.setPassword(password);

		Gson gson = new Gson();

		ResultActions result = mockMvc
				.perform(post("/login").content(gson.toJson(user)).accept("application/json;charset=UTF-8"))
				.andExpect(status().isOk());

		String resultString = result.andReturn().getResponse().getContentAsString();

		return resultString;
	}
}
