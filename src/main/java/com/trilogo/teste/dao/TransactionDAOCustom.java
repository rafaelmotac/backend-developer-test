package com.trilogo.teste.dao;

import java.util.List;

import com.trilogo.teste.model.Purchase;

public interface TransactionDAOCustom {

    List<Purchase> listAllPurchasesByClientId(String clientId);
}