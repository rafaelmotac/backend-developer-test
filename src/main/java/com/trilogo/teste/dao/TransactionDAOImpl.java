package com.trilogo.teste.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.trilogo.teste.model.Purchase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class TransactionDAOImpl implements TransactionDAOCustom {

    @Autowired
    private EntityManager entityManager;

    @Override
    @Transactional(propagation=Propagation.NOT_SUPPORTED)
	public List<Purchase> listAllPurchasesByClientId(String clientId) {

        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT new com.trilogo.teste.model.Purchase(t.clientId, t.id, t.totalToPay, t.dateTransaction, t.creditCard.card_number) ");
        jpql.append("FROM Transaction t ");
        jpql.append("WHERE t.clientId = :clientId");

        TypedQuery<Purchase> query = entityManager.createQuery(jpql.toString(), Purchase.class);
        query.setParameter("clientId", clientId);

		List<Purchase> resultList = query.getResultList();
		return resultList;
	}
    
}