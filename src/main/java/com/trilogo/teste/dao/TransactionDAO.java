package com.trilogo.teste.dao;

import java.util.List;

import com.trilogo.teste.model.Purchase;
import com.trilogo.teste.model.Transaction;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface TransactionDAO extends CrudRepository<Transaction, Long>, TransactionDAOCustom {

    public List<Transaction> findByClientId(String clientId);


    @Query("SELECT new com.trilogo.teste.model.Purchase(t.clientId, t.id, t.totalToPay, t.dateTransaction, t.creditCard.card_number) FROM Transaction t")
    public List<Purchase> listAllPurchases();

}