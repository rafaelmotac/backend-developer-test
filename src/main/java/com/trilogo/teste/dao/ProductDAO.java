package com.trilogo.teste.dao;

import com.trilogo.teste.model.Product;

import org.springframework.data.repository.CrudRepository;

public interface ProductDAO extends CrudRepository<Product, Long> {
    
}