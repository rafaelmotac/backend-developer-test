package com.trilogo.teste.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.trilogo.teste.dao.TransactionDAO;
import com.trilogo.teste.model.Purchase;
import com.trilogo.teste.model.Transaction;

@Service
public class TransactionService {

    @Autowired
    private TransactionDAO transactionDAO;

    public Transaction saveTransaction(Transaction transaction){
    	transaction.setDateTransaction(new Date());
        return transactionDAO.save(transaction);
    }
    
    @Transactional(propagation=Propagation.NOT_SUPPORTED)
    public List<Purchase> listAllPurchases(){
        return transactionDAO.listAllPurchases();
    }

    @Transactional(propagation=Propagation.NOT_SUPPORTED)
    public List<Purchase> listPurchasesByClientId(String clientId){
        return transactionDAO.listAllPurchasesByClientId(clientId);
    }
   

}