package com.trilogo.teste.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.trilogo.teste.dao.ProductDAO;
import com.trilogo.teste.model.Product;

@Service
public class ProductService {

    @Autowired
    private ProductDAO productDAO;


    @Transactional(propagation=Propagation.NOT_SUPPORTED)
    public List<Product> listAllProducts(){
        return (List<Product>) productDAO.findAll();
    }

    public Product saveProduct(Product product){
        return productDAO.save(product);
    }

}