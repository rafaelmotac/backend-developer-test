package com.trilogo.teste.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.trilogo.teste.model.Product;
import com.trilogo.teste.model.Purchase;
import com.trilogo.teste.model.Transaction;
import com.trilogo.teste.service.ProductService;
import com.trilogo.teste.service.TransactionService;

@RestController
public class StarStoreController {

	@Autowired
	private ProductService productService;

	@Autowired
	private TransactionService transactionService;

	@Transactional
	@ResponseBody
	@PostMapping(value = "/starstore/product")
	public ResponseEntity<Product> saveProduct(@RequestBody(required = true) Product product) {

		return new ResponseEntity<Product>(productService.saveProduct(product), HttpStatus.OK);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@GetMapping(value = "/starstore/products", produces = "application/json;charset=UTF-8")
	public List<Product> getProducts() {
		return productService.listAllProducts();
	}

	@Transactional
	@ResponseBody
	@PostMapping(value = "/starstore/buy")
	public ResponseEntity<Transaction> saveTransaction(@RequestBody(required = true) Transaction transaction) {

		Transaction result = transactionService.saveTransaction(transaction);

		return new ResponseEntity<Transaction>(result, HttpStatus.OK);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@GetMapping(value = "/starstore/history", produces = "application/json;charset=UTF-8")
	public List<Purchase> getHistory() {
		return transactionService.listAllPurchases();
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@GetMapping(value = "/starstore/history/{clientId}", produces = "application/json;charset=UTF-8")
	public List<Purchase> getHistoryByClientId(@PathVariable(name = "clientId", required = true) String clientId) {
		return transactionService.listPurchasesByClientId(clientId);
	}

}