package com.trilogo.teste.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Purchase implements Serializable {

	private static final long serialVersionUID = 8196553615993983698L;

	@JsonProperty("client_id")
	private String clientId;

	@JsonProperty("purchase_id")
	private String purchaseId;

	@JsonProperty
	private Integer value;

	@JsonProperty
	private String date;

	@JsonProperty("card_number")
	private String cardNumber;

	public Purchase() {
	}

	public Purchase(String clientId, Long purchaseId, Integer value, Date date, String cardNumber) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		this.clientId = clientId;
		this.purchaseId = purchaseId.toString();
		this.value = value;
		this.date = sdf.format(date.getTime());
		this.cardNumber = cardNumber;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCardNumber() {
		String mask = this.cardNumber.replaceAll("\\w(?=\\w{4})", "*");
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < mask.length(); i++) {
			if (i % 4 == 0 && i != 0) {
				result.append(" ");
			}

			result.append(mask.charAt(i));
		}
		return result.toString();
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

}